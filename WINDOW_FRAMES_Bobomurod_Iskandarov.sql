-- CTE for aggregating daily sales data
WITH SalesData AS (
    SELECT
        s.time_id,
        t.day_name,
        t.calendar_week_number,
        -- Sum of daily sales
        SUM(s.amount_sold) AS daily_sales
    FROM
        sh.sales s
    JOIN
        sh.times t ON s.time_id = t.time_id
    WHERE
        t.calendar_year = 1999 AND
        t.calendar_week_number IN (49, 50, 51)
    GROUP BY
        s.time_id, t.day_name, t.calendar_week_number
),


-- CTE for calculating cumulative sales
CumulativeSales AS (
    SELECT
        time_id,
        day_name,
        calendar_week_number,
        daily_sales,
        SUM(daily_sales) OVER (
            PARTITION BY calendar_week_number
            ORDER BY time_id
            ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
        ) AS CUM_SUM
    FROM
        SalesData
),


-- CTE for calculating centered 3-day average with adjusted window frames
CenteredAvg AS (
    SELECT
        *,
        
        CASE
            WHEN day_name = 'Monday' AND calendar_week_number = 49 THEN AVG(daily_sales) OVER (
                
                ORDER BY time_id
                ROWS BETWEEN CURRENT ROW AND 1 FOLLOWING
            )
            WHEN day_name = 'Friday' AND calendar_week_number = 51 THEN AVG(daily_sales) OVER (
                
                ORDER BY time_id
                ROWS BETWEEN 1 PRECEDING AND CURRENT ROW
            )
            WHEN day_name = 'Monday' THEN AVG(daily_sales) OVER (
                
                ORDER BY time_id
                ROWS BETWEEN 2 PRECEDING AND 1 FOLLOWING
            )
            WHEN day_name = 'Friday' THEN AVG(daily_sales) OVER (
                
                ORDER BY time_id
                ROWS BETWEEN 1 PRECEDING AND 2 FOLLOWING
            )
            ELSE AVG(daily_sales) OVER (
                
                ORDER BY time_id
                ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING
            )
        END AS CENTERED_3_DAY_AVG
    FROM
        CumulativeSales
)

-- Selection of required columns
SELECT
    time_id,
    day_name,
    calendar_week_number,
    daily_sales,
    CUM_SUM,
    CENTERED_3_DAY_AVG
FROM
    CenteredAvg
-- Ordering by time_id to show chronological order
ORDER BY
    time_id;